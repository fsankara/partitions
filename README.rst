Musical Partition Management
============================

This is a extension for TYPO3 CMS 6.2 and above which provides a bunch of Domain Model for handling a set of Partitions.

Provides a bunch of Domain Model for handling a set of musical Partitions.

Installing the extension
------------------------

Install in the Extension Manager as normal.

After that, **load the static TS** of the extension in your root TS template (AKA sys_template record).


Load assets files (JS / CSS)
----------------------------

In order the plugin to work, it is required to load some CSS and JavaScript.

	# CSS
	EXT:partitions/Resources/Public/StyleSheets/partitions.css

	# JavaScript
	EXT:partitions/Resources/Public/JavaScript/partitions.js

Scripts
-------

There is a script to deploy on the server since there is not Git available on the production.

::

	cd Scripts
	./deploy.bash


TODO
====

* Create Partials for better maintainability
* Fix display of categories
* Invert arrow in collapse
* Invert arrow in collapse
* Configure BE module the same way as on the Frontend
* Check whether to use HTML5 audio.

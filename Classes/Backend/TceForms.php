<?php
namespace Hemmer\Partitions\Backend;

/**
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
use TYPO3\CMS\Backend\Form\FormEngine;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * A class to interact with TCEForms
 */
class TceForms {

	/**
	 * This method modifies the list of items for FlexForm "dataType".
	 *
	 * @param array $parameters
	 * @param FormEngine $parentObject
	 * @return string
	 */
	public function renderComboBox(&$parameters, $parentObject = NULL) {
		$output = sprintf(
			'<input list="list-%s" name="%s" value="%s" maxlength="256" style="width: 404px;"/> %s',
			$parameters['field'],
			$parameters['itemFormElName'],
			$parameters['itemFormElValue'],
			$this->getDataList($parameters['field'])
		);
		return $output;
	}

	/**
	 * @param string $fieldName
	 * @return array
	 */
	protected function getDataList($fieldName) {

		$dataList = array();

		foreach ($this->getDistinctRecords($fieldName) as $record) {
			$dataList[] = sprintf('<option value="%s">', $record[$fieldName]);
		}

		return sprintf('<datalist id="list-%s">%s</datalist>',
			$fieldName,
			implode("\n", $dataList)
		);
	}

	/**
	 * @param string $fieldName
	 * @return array
	 */
	protected function getDistinctRecords($fieldName) {
		$tableName = 'tx_partitions_domain_model_partition';
		$clause = sprintf('%s != "" AND %s != "0" ', $fieldName, $fieldName);
		$clause .= BackendUtility::deleteClause($tableName);
		return $this->getDatabaseConnection()->exec_SELECTgetRows('DISTINCT ' . $fieldName, $tableName, $clause, '', 'title ASC');
	}

	/**
	 * Returns the TypoScript configuration for this extension.
	 *
	 * @return array
	 */
	protected function getPluginConfiguration() {
		$setup = $this->getConfigurationManager()->getTypoScriptSetup();
		$extensionName = 'partitions';

		$pluginConfiguration = array();
		if (is_array($setup['plugin.']['tx_' . strtolower($extensionName) . '.'])) {
			/** @var \TYPO3\CMS\Extbase\Service\TypoScriptService $typoScriptService */
			$typoScriptService = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Service\TypoScriptService');
			$pluginConfiguration = $typoScriptService->convertTypoScriptArrayToPlainArray($setup['plugin.']['tx_' . strtolower($extensionName) . '.']);
		}
		return $pluginConfiguration;
	}

	/**
	 * Returns a pointer to the database.
	 *
	 * @return \TYPO3\CMS\Core\Database\DatabaseConnection
	 */
	protected function getDatabaseConnection() {
		return $GLOBALS['TYPO3_DB'];
	}

}
<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

return array(
	'ctrl' => array(
		'title' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:partition',
		'label' => 'title',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'cruser_id' => 'cruser_id',
		'dividers2tabs' => TRUE,

		'versioningWS' => 2,
		'versioning_followPages' => TRUE,

		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete' => 'deleted',
		'enablecolumns' => array(
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime',
		),
		'searchFields' => 'title,composer',
		'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('partitions') . 'Resources/Public/Icons/tx_partitions_domain_model_partition.gif'
	),
	'types' => array(
		#sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource
		'1' => array('showitem' => 'title, composer, composer_origin, year_of_composition, formation, theme, modification_date,
			author, arranger, publication_year, editor, reference, duration, remark, liturgy_function, comment, internal_identifier,
			difficulty, comment_difficulty, writing_quality, comment_writing_quality, voice, liturgical_season, accompaniment, video,
			--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.audio, audio_files,
			--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.partitions, partition_files,
			--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.contributors, contributors,
			--div--;LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:tabs.relations, related_partitions,
			--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, hidden, starttime, endtime'
		),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_partitions_domain_model_partition',
				'foreign_table_where' => 'AND tx_partitions_domain_model_partition.pid=###CURRENT_PID### AND tx_partitions_domain_model_partition.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'title' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim,required'
			),
		),
		'composer' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'composer_origin' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:composer_origin',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'year_of_composition' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:year_of_composition',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'formation' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:formation',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'theme' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:theme',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'modification_date' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:modification_date',
			'config' => array(
				'dbType' => 'datetime',
				'type' => 'input',
				'size' => 12,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => '0000-00-00 00:00:00'
			),
		),
		'author' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:author',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'arranger' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:arranger',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'publication_year' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:publication_year',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'editor' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:editor',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'reference' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:reference',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'duration' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:duration',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'remark' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:remark',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'liturgy_function' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:liturgy_function',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'comment_writing_quality' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:comment_writing_quality',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 3,
				'eval' => 'trim'
			),
		),
		'comment_difficulty' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:comment_difficulty',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 3,
				'eval' => 'trim'
			),
		),
		'internal_identifier' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:internal_identifier',
			'config' => array(
				'type' => 'input',
				'size' => 4,
				'eval' => 'int'
			)
		),
		'partition_files' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:partition_files',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'partitions',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:addPartitionFileReference'
					),
					'maxitems' => 1
				),
				'pdf'
			),
		),
		'audio_files' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:audio_files',
			'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
				'files',
				array(
					'appearance' => array(
						'createNewRelationLinkTitle' => 'LLL:EXT:partitions/Resources/Private/Language/locallang.xlf:addAudioFileReference'
					),
					'maxitems' => 1
				),
				'mp3,midi'
			),
		),
		'difficulty' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:difficulty',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', ''),
					array('* * * * *', 5),
					array('* * * *', 4),
					array('* * *', 3),
					array('* *', 2),
					array('*', 1),
				),
				'maxitems' => 1,
				'minitems' => 0,
			),
		),
		'writing_quality' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:writing_quality',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', ''),
					array('* * * * *', 5),
					array('* * * *', 4),
					array('* * *', 3),
					array('* *', 2),
					array('*', 1),
				),
				'maxitems' => 1,
				'minitems' => 0,
			)
		),
		'voice' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:voice',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'liturgical_season' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:liturgical_season',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'accompaniment' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:accompaniment',
			'config' => array(
				'type' => 'user',
				'userFunc' => 'Hemmer\Partitions\Backend\TceForms->renderComboBox',
				'eval' => 'trim'
			),
		),
		'video' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:video',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'related_partitions' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:related_partitions',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'tx_partitions_domain_model_partition',
				'MM' => 'tx_partitions_partition_partition_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
				'wizards' => array(
					'suggest' => array(
						'type' => 'suggest',
					),
					'edit' => array(
						'type' => 'popup',
						'title' => 'Edit',
						'script' => 'wizard_edit.php',
						'icon' => 'edit2.gif',
						'popup_onlyOpenIfSelected' => 1,
						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
					),
					'add' => Array(
						'type' => 'script',
						'title' => 'Create new',
						'icon' => 'add.gif',
						'params' => array(
							'table' => 'tx_partitions_domain_model_partition',
							'pid' => '###CURRENT_PID###',
							'setValue' => 'prepend'
						),
						'script' => 'wizard_add.php',
					),
				),
			),
		),
		'contributors' => array(
			'label' => 'LLL:EXT:partitions/Resources/Private/Language/tx_partitions_domain_model_partition.xlf:contributors',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_partitions_domain_model_contributor',
				'MM' => 'tx_partitions_partition_contributor_mm',
				'size' => 10,
				'autoSizeMax' => 30,
				'maxitems' => 9999,
				'multiple' => 0,
//				'wizards' => array(
//					'edit' => array(
//						'type' => 'popup',
//						'title' => 'Edit',
//						'script' => 'wizard_edit.php',
//						'icon' => 'edit2.gif',
//						'popup_onlyOpenIfSelected' => 1,
//						'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
//					),
//					'add' => Array(
//						'type' => 'script',
//						'title' => 'Create new',
//						'icon' => 'add.gif',
//						'params' => array(
//							'table' => 'tx_partitions_domain_model_contributor',
//							'pid' => '###CURRENT_PID###',
//							'setValue' => 'prepend'
//						),
//						'script' => 'wizard_add.php',
//					),
//				),
			),
		),
	),
	'grid' => array(
		'facets' => array(
			'uid',
			'title',
		),
		'columns' => array(
			'__checkbox' => array(
				'renderer' => new \TYPO3\CMS\Vidi\Grid\CheckBoxComponent(),
			),
			'uid' => array(
				'visible' => FALSE,
				'label' => 'Id',
				'width' => '5px',
			),
			'title' => array(
				'editable' => TRUE,
			),
			'__buttons' => array(
				'renderer' => new \TYPO3\CMS\Vidi\Grid\ButtonGroupComponent(),
			),
		)
	),

	'grid_frontend' => array(
		'columns' => array(

			# The field "title" of your table.
			'title' => array(),
			'voice' => array(),
			'author' => array(),
			'composer' => array(),
			'arranger' => array(),
			#'partition_files' => array(),
			#'audio_files' => array(),
			#'contributors' => array(),
			'video' => array(),
			'writing_quality' => array(),

			# System column where to contain some
			'__buttons' => array(
				'renderer' => 'Fab\VidiFrontend\Grid\ShowButtonRenderer',
				'sortable' => FALSE
			),
		),
	),

);

